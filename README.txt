// $Id $

The Quotations module provides images and CSS that allows you to quickly stylize quotations that appear on your site.

Quotations was designed by Mark W. Jarrell (attheshow) at Fleet Thought (http://fleetthought.com).


Install
-------
Installing the Quotations module is simple:

1) Copy the quotations folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules (/admin/build/modules)

3) Configure the module so that it works as with your theme at Administer -> Site configuration -> Quotations (/admin/settings/quotations)


Customization
-------

Quotations should work fine with most themes, but needs to be configured so that the included CSS will affect the specific CSS classes that you want it to work on. Please use the administration form for
Quotations to customize. More customization tips can be found in the help for the module.
You'll find the help at /admin/help/quotations.

Support
-------
If you experience a problem with the module, file a
request or issue on the Quotations queue. DO NOT POST IN THE FORUMS. Posting in
the issue queues is a direct line of communication with the module author.